package hyhtool

import (
	"crypto/md5"
	"fmt"
	"github.com/axgle/mahonia"
	"io"
	"log"
	"strconv"
	"strings"
)

// 反转字符串
func ReverseString(s string) string {
	runes := []rune(s)
	for from, to := 0, len(runes)-1; from < to; from, to = from+1, to-1 {
		runes[from], runes[to] = runes[to], runes[from]
	}
	return string(runes)
}

func SplitString(str string, sep string)[]string{
	splits := strings.Split(str, sep)
	for i, size := 0, len(splits); i < size; {
		v := splits[i]
		if len(v) <= 0{
			// 删除这个
			splits = append(splits[:i], splits[i+1:]...)
			size--
		}else{
			i++
		}
	}
	return splits
}

//方式一
func GetMd5String1(str string) string {
	m := md5.New()
	_, err := io.WriteString(m, str)
	if err != nil {
		log.Fatal(err)
	}
	arr := m.Sum(nil)
	return fmt.Sprintf("%x", arr)
}

//方式二
func GetMd5String2(b []byte) string {
	return fmt.Sprintf("%x", md5.Sum(b))
}

func GbkToUtf8(strGbk string) string{
	dec := mahonia.NewDecoder("gbk")
	return dec.ConvertString(strGbk)
}

func Utf8ToGbk(strUtf8 string) string{
	enc := mahonia.NewEncoder("gbk")
	return enc.ConvertString(strUtf8)
}

// Capitalize 字符首字母大写
func Capitalize(str string) string {
	var upperStr string
	vv := []rune(str)   // 后文有介绍
	for i := 0; i < len(vv); i++ {
		if i == 0 {
			if vv[i] >= 97 && vv[i] <= 122 {  // 后文有介绍
				vv[i] -= 32 // string的码表相差32位
				upperStr += string(vv[i])
			} else {
				fmt.Println("Not begins with lowercase letter,")
				return str
			}
		} else {
			upperStr += string(vv[i])
		}
	}
	return upperStr
}

func TrimMyStr(str string) string{
	nStrLen := len(str)
	lastNoneZero := nStrLen
	for i:=nStrLen-1;i>=0;i--{
		if str[i] != 0{
			lastNoneZero = i
			break
		}
	}
	strRet := str
	if lastNoneZero < nStrLen{
		strRet = str[:lastNoneZero+1]
	}
	strRet = strings.ReplaceAll(strRet,"\t"," ")
	strRet = strings.Trim(strRet, " ")
	return strRet
}

func ReplaceAllBlank(str string) string{
	strRet := strings.ReplaceAll(str,"\t"," ")
	strRet = strings.ReplaceAll(strRet, " ","")
	return strRet
}

func InetAddr(ip string) int{
	ipSegs := strings.Split(ip, ".")
	var ipInt int = 0
	var pos uint = 24
	for _, ipSeg := range ipSegs {
		ipSeg = TrimMyStr(ipSeg)
		tempInt, _ := strconv.Atoi(ipSeg)
		tempInt = tempInt << pos
		ipInt = ipInt | tempInt
		pos -= 8
	}
	return ipInt
}

func InetNtoA(ip int32) string{
	return fmt.Sprintf("%d.%d.%d.%d",
		byte(ip>>24), byte(ip>>16), byte(ip>>8), byte(ip))
}