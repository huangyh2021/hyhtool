package hyhtool

import (
	"encoding/json"
	"strconv"
)

type HJson  map[string]interface{}

func MakeHJson() HJson{
	return make(map[string]interface{})
}

//字符串转换为json数组
func ParseStrToArray(str string)([]HJson,error){
	tempSlice := make([]HJson,0)
	err := json.Unmarshal([]byte(str), &tempSlice)
	if err != nil {
		return nil,err
	}else {
		return tempSlice,nil
	}
}

//slice转json
func ArrayToJsonStr(s []HJson) (string,error) {
	data, err := json.Marshal(s)
	if err != nil {
		return "",err
	}
	return string(data),nil
}

//map转json
func HJsonToStr(tempMap HJson) (string,error){
	data, err := json.Marshal(tempMap)
	if err != nil {
		return "",err
	}else{
		return string(data),nil
	}
}

func ParseStrToHJson(str string) (HJson, error){
	tempMap := MakeHJson()
	err := json.Unmarshal([]byte(str), &tempMap)
	if err != nil {
		return nil,err
	}else {
		return tempMap,nil
	}
}

func GetJsonInt(json HJson, key string, default_value int) int{
	if v, ok := json[key]; ok{
		switch v.(type) {
		case int:
			return v.(int)
		case string:
			i,err := strconv.Atoi(v.(string))
			if(err == nil){
				return i
			}else{
				return default_value
			}
		case float64:
			return int(v.(float64))
		default:
			return default_value
		}
	}else{
		return default_value
	}
}

func GetJsonFloat(json HJson, key string, default_value float64) float64{
	if v, ok := json[key]; ok{
		switch v.(type) {
		case int:
			return float64(v.(int))
		case string:
			i,err := strconv.ParseFloat(v.(string),64)
			if(err == nil){
				return i
			}else{
				return default_value
			}
		case float64:
			return v.(float64)
		default:
			return default_value
		}
	}else{
		return default_value
	}
}

func GetJsonStr(json HJson, key string,default_value string) string{
	if v, ok := json[key]; ok{
		switch v.(type) {
		case string:
			return v.(string)
		default:{
			return default_value
		}
		}
	}else{
		return default_value
	}
}

func GetJsonArray(json HJson, key string) []interface{}{
	if v, ok := json[key]; ok{
		switch v.(type) {
		case []interface{}:
			return v.([]interface{})
		default:{
			return nil
		}
		}
	}else{
		return nil
	}
}

func GetJsonObject(json HJson, key string) HJson{
	if v, ok := json[key]; ok{
		switch v.(type) {
		case interface{}:
			return v.(HJson)
		default:{
			return nil
		}
		}
	}else{
		return nil
	}
}

func ToJsonString(json HJson)string{
	v,err := HJsonToStr(json)
	if err == nil{
		return v
	}else{
		return ""
	}
}
