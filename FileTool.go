package hyhtool

import (
	"fmt"
	"os"
)

/**
 * 判断文件是否存在  存在返回 true 不存在返回false
 */
func CheckFileIsExist(filename string) bool {
	var exist = true
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		exist = false
	}
	return exist
}

/**
 * 打开文件  成功返回 不存在返回false
 */
func OpenMyFile(filename string) *os.File{
	if CheckFileIsExist(filename) { //如果文件存在
		f, err1 := os.OpenFile(filename, os.O_CREATE, 0666) //打开文件
		if err1 != nil{
			fmt.Println("文件存在")
		}
		return f
	} else {
		f, err1 := os.Create(filename) //创建文件
		if err1 != nil {
			fmt.Println("文件不存在")
		}
		return f
	}
}

func CloseMyFile(f *os.File){
	if f == nil{
		return
	}
	f.Close()
}

func SaveDataToFile(data []byte, f*os.File) (int,error){
	//buf := new(bytes.Buffer)
	//size := len(data)
	//sizeA := Int2Byte(size)
	//binary.Write(buf,binary.LittleEndian,sizeA)
	//binary.Write(buf, binary.LittleEndian, data)
	//return f.Write(buf.Bytes())
	return f.Write(data)
}

func ReadDataFromFile(buf []byte,f *os.File ) int{
	len, err := f.Read(buf)
	if err != nil{
		return -1
	}else{
		return len
	}

}