package hyhtool

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"net/http"
)

type Exe_Info struct{
	Uid		string
	Pid		int
	Ppid	int
	Cpu     float32
	Mem     int
	Cmd  	string
	Params  []string
}

// Define ternary operate function
func ternary(expr bool, whenTrue, whenFalse interface{}) interface{} {
	if expr == true {
		return whenTrue
	}
	return whenFalse
}

func MIN(a, b int) int {
	i := ternary(a <= b, a, b)
	r, _ := i.(int)
	return r
}

func MAX(a, b int) int {
	i := ternary(a >= b, a, b)
	r, _ := i.(int)
	return r
}

func GetTime() int64{
	i64Now := time.Now().UnixNano()
	return int64(i64Now/(1000*1000))
}

func GetExecutePath() string {
	dir, err := os.Executable()
	if err != nil {
		fmt.Println(err)
		return ""
	}
	exPath := filepath.Dir(dir)
	return exPath
}

func GetMyMac() string {
	// 获取本机的MAC地址
	interfaces, err := net.Interfaces()
	if err != nil {
		panic("Poor soul, here is what you got: " + err.Error())
	}

	inter := interfaces[0]
	mac := strings.Replace(inter.HardwareAddr.String(), ":", "-", -1) //获取本机MAC地址
	mac = mac+"-00-00"
	return mac
}

func SetCommandStd(cmd *exec.Cmd) (stdout, stderr *bytes.Buffer) {
	stdout = &bytes.Buffer{}
	stderr = &bytes.Buffer{}
	cmd.Stdout = stdout
	cmd.Stderr = stderr
	return
}

func GetExeInfo(strExe string) []Exe_Info{
	var retPid = make([]Exe_Info,0)
	psCmd := exec.Command("ps", "-ef")
	psStdout, psStderr := SetCommandStd(psCmd)
	err := psCmd.Run()
	if err != nil {
		err = errors.New(err.Error() + psStderr.String())
		return retPid
	}
	// 筛选
	grepCmd := exec.Command("grep", strExe)
	grepCmd.Stdin = psStdout
	grepStdout, grepStderr := SetCommandStd(grepCmd)
	err = grepCmd.Run()
	if err != nil {
		err = errors.New(err.Error() + grepStderr.String())
		return retPid
	}
	exes := strings.Split(grepStdout.String(),"\n")
	if len(exes) > 0 {
		for _,v := range exes {
			count := 0
			if strings.Index(v, "defunct")>=0{	// 忽略僵尸进程
				continue
			}
			seps := strings.Split(v, " ")
			var info Exe_Info
			info.Params = make([]string,0)
			for i := 0; i < len(seps); i++ {
				section := seps[i]
				if len(section) <= 0{
					continue
				}
				switch count{
				case 0:
					info.Uid = section
				case 1:
					pid,err := strconv.Atoi(section)
					if err == nil{
						info.Pid = pid
					}
				case 2:
					ppid,err := strconv.Atoi(section)
					if err == nil{
						info.Ppid = ppid
					}
				case 3:
					cpu,err := strconv.Atoi(section)
					if err == nil{
						info.Cpu = float32(cpu)
					}
				case 4,5,6:
					// I don't care
				case 7:
					info.Cmd = section
				default:
					info.Params = append(info.Params, section)
				}
				count++
			}
			if info.Pid > 0 && strings.Index(info.Cmd,strExe) >= 0{
				retPid = append(retPid,info)
			}
		}
	}
	return retPid
}

func GetExeMem(pid int) int{
	file_name := fmt.Sprintf("/proc/%d/status", pid)
	b, err := ioutil.ReadFile(file_name)
	if err != nil {
		fmt.Printf("Error: read file %s fail! err=%v\n", file_name, err)
		return -1
	}
	str := string(b)
	params := strings.Split(str,"\n")
	for i:= 0;i< len(params);i++{
		line := params[i]
		if strings.Index(line, "VmRSS:") >= 0{
			value := line[6:]
			end := strings.Index(params[i],"kB")
			if end > 0{
				value = line[6:end-1]
			}
			value = strings.Trim(value,"\t")
			value = strings.Trim(value," ")
			ret,err := strconv.Atoi(value)
			if err != nil{
				return -3
			}else{
				return ret
			}
		}
	}
	return 0
}

func GetTopInfo() ([]Exe_Info, []string){
	var retPid = make([]Exe_Info,0)
	var strTitle = make([]string,0)
	psCmd := exec.Command("top",  "-b", "-n", "1")
	psStdout, psStderr := SetCommandStd(psCmd)
	err := psCmd.Run()
	if err != nil {
		err = errors.New(err.Error() + psStderr.String())
		//fmt.Println(err)
		return retPid,strTitle
	}
	strOut := psStdout.String()
	exes := strings.Split(strOut,"\n")
	if len(exes) > 0 {
		for i,v := range exes {
			if i <= 6{
				if i <= 5{
					strTitle = append(strTitle, v)
				}
				continue
			}
			count := 0
			seps := strings.Split(v, " ")
			var info Exe_Info
			info.Params = make([]string,0)
			for j := 0; j < len(seps); j++ {
				section := seps[j]
				if len(section) <= 0{
					continue
				}
				switch count{
				case 0:
					pid,err := strconv.Atoi(section)
					if err == nil{
						info.Pid = pid
					}
				case 1:
					info.Uid = section
				case 2,3,4:
					// I don't care
				case 5:
					mem,err := strconv.Atoi(section)
					if err == nil{
						info.Mem = mem
					}
				case 6,7:
					// I don't care
				case 8:
					cpu,err := strconv.ParseFloat(section, 32)
					if err == nil{
						info.Cpu = float32(cpu)
					}
				case 9:
					// % mem
				case 10:
					// time
				case 11:
					info.Cmd = section
				default:
					info.Params = append(info.Params, section)
				}
				count++
			}
			if info.Cmd != "top"{
				retPid = append(retPid,info)
			}

		}
	}
	return retPid,strTitle
}

func Redirect(w http.ResponseWriter, req *http.Request, url string) {
	target := url
	http.Redirect(w, req, target,
		// see @andreiavrammsd comment: often 307 > 301
		http.StatusTemporaryRedirect)
}