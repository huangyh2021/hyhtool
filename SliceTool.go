package hyhtool

import (
	"encoding/binary"
	"math"
)

func ParseByteFromSlice(ary []byte, start int, default_val byte) byte{
	if start < 0 || start + 1 > len(ary){
		return default_val
	}
	return ary[start]
}

func ParseIntFromSlice(ary []byte, start int, default_val int) int{
	if start < 0 || start + 4 > len(ary){
		return default_val
	}
	bytes := ary[start:start+4]
	return int(binary.LittleEndian.Uint32(bytes))
}

func ParseInt32FromSlice(ary []byte, start int, default_val int32) int32{
	if start < 0 || start + 4 > len(ary){
		return default_val
	}
	bytes := ary[start:start+4]
	return int32(binary.LittleEndian.Uint32(bytes))
}

func ParseInt64FromSlice(ary []byte, start int, default_val int64) int64{
	if start < 0 || start + 8 > len(ary){
		return default_val
	}
	bytes := ary[start:start+8]
	return int64(binary.LittleEndian.Uint64(bytes))
}

func ParseInt16FromSlice(ary []byte, start int, default_val int16) int16{
	if start < 0 || start + 2 >= len(ary){
		return default_val
	}
	bytes := ary[start:start+2]
	return int16(binary.LittleEndian.Uint16(bytes))
}

func ParseFloat32FromSlice(ary []byte, start int, default_val float32) float32{
	if start < 0 || start + 4 > len(ary){
		return default_val
	}
	bytes := ary[start:start+4]
	return float32(binary.LittleEndian.Uint32(bytes))
}

func ParseFloat64FromSlice(ary []byte, start int, default_val float64) float64{
	if start < 0 || start + 8 > len(ary){
		return default_val
	}
	bytes := ary[start:start+8]
	return float64(binary.LittleEndian.Uint64(bytes))
}

func ParseStringFromSlice(ary []byte, start int, count int) string{
	if start < 0 || start + count >= len(ary){
		return "ParseStringError"
	}
	return string(ary[start:start+count])
}

func Int2Byte(data int) [4]byte{
	var b3 [4]byte
	b3[0] = uint8(data)
	b3[1] = uint8(data >> 8)
	b3[2] = uint8(data >> 16)
	b3[3] = uint8(data >> 24)
	return b3
}

func CopyByteToSlice(ary []byte, i byte){
	ary[0] = i
}

func CopyInt16ToSlice(ary []byte, i int16){
	binary.LittleEndian.PutUint16(ary,uint16(i))
}

func CopyInt32ToSlice(ary []byte, i int32){
	binary.LittleEndian.PutUint32(ary,uint32(i))
}

func CopyInt64ToSlice(ary []byte, i int64){
	binary.LittleEndian.PutUint64(ary,uint64(i))
}

func CopyFloat32ToSlice(ary []byte, f float32){
	bits := math.Float32bits(f)
	binary.LittleEndian.PutUint32(ary,bits)
}

func CopyFloat64ToSlice(ary []byte, f float64){
	bits := math.Float64bits(f)
	binary.LittleEndian.PutUint64(ary,bits)
}
