package hyhtool

import (
	"fmt"
	"io/ioutil"
	"net"
	"strings"
	"sync"
	"time"
)

type UdpLog struct{
	port 	int
	run		chan int
	status	int
	logPath string
	lock    sync.Mutex
	addr    *net.UDPAddr
	sockUdp *net.UDPConn
}

func NewUdpLog(port int, logPath string) *UdpLog{
	log := new(UdpLog)
	log.port = port
	log.logPath = logPath
	log.run = make(chan int, 1)
	if len(logPath) > 0 {
		data, err := ioutil.ReadFile(logPath)
		if err == nil {
			jsonData, err := ParseStrToHJson(string(data))
			if err == nil {
				ip := GetJsonStr(jsonData, "ip", "")
				port := GetJsonInt(jsonData, "port", 0)
				if len(ip) > 0 && port > 0 && port < 65535 {
					addr, err := net.ResolveUDPAddr("udp4", fmt.Sprintf("%s:%d", ip, port))
					if err == nil {
						log.addr = addr
					}
				}
			}
		}
	}

	go log.listenUdpProcess()
	return log
}

func FreeUdpLog(log* UdpLog){
	log.lock.Lock()
	if log.sockUdp != nil{
		log.sockUdp.Close()
	}
	log.lock.Unlock()
}

func (log *UdpLog)WriteLog(data []byte){
	fmt.Println(string(data))
	log.lock.Lock()
	if log.sockUdp != nil && log.addr != nil{
		log.sockUdp.WriteToUDP(data, log.addr)
	}
	log.lock.Unlock()
}

func (log *UdpLog)WriteLogStr(str string){
	fmt.Println(str)

	log.lock.Lock()
	if log.sockUdp != nil && log.addr != nil{
		log.sockUdp.WriteToUDP([]byte(str), log.addr)
	}
	log.lock.Unlock()
}

func (log *UdpLog)listenUdpProcess(){
	ticker := time.NewTicker(10 * time.Millisecond)
	defer ticker.Stop()
	var buffer [64*1024]byte

	sockUdp, err := net.ListenUDP("udp4", &net.UDPAddr{
		IP:   net.ParseIP("0.0.0.0"),
		Port: log.port,
	})
	if err != nil {
		fmt.Println("初始化UDP端口失败!", err)
		return
	}
	defer sockUdp.Close()
	log.sockUdp = sockUdp
	log.status = 1
	for log.status > 0 {
		recvLen,rAddr,err := sockUdp.ReadFromUDP(buffer[0:])
		if err != nil{
			break
		}
		strCmd := string(buffer[0:recvLen])
		indexCmd := strings.Index(strCmd,"mics_cmd:")
		if indexCmd >= 0{
			fmt.Printf("recv udp cmd(%s) :%s\n",rAddr.String(), strCmd)
			cmd := strCmd[9:]
			if strings.Contains(cmd,"getlog"){
				if strings.Contains(cmd, "off"){
					log.lock.Lock()
					log.addr = nil
					log.lock.Unlock()
				}else{
					log.lock.Lock()
					log.addr = rAddr
					log.lock.Unlock()

					log.WriteLogStr("begin send cmd\n")

					if len(log.logPath) > 0{
						jsonData := MakeHJson()
						jsonData["ip"] = rAddr.IP
						jsonData["port"] = rAddr.Port
						strSave ,err := HJsonToStr(jsonData)
						if err ==nil{
							ioutil.WriteFile(log.logPath, []byte(strSave), 0666)
						}
					}

				}
			}
		}
	}
	log.status = 0
}
